import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';
  name: string = '';
  app: string = '';
  res: any = {
    name: '',
    app: '',
  };

  onSave() {
    console.log();
    this.res = {
      nae: this.name,
      app: this.app,
    };
    console.log(this.res);
  }
}